#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
x = [1,3,2,4]
y = [1,2,3,4]

plt.plot(x,y)
plt.xlim(0,6)
plt.ylim(0,5)
plt.show()
