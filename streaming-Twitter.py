#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#https://github.com/bear/python-twitter
import twitter

#import/lookup credentials
api = twitter.Api(consumer_key = twitter_consumer_key,
                  consumer_secret = twitter_consumer_secret,
                  access_token_key = twitter_access_token,
                  access_token_secret = twitter_access_token)

print(api.VerifyCredentials())

keywords = ["Yahoo", "YHOO"]

for eachTweet in api.GetStreamFilter(track=keywords):
    print(eachTweet)
    break