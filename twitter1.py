#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import requests
import requests_oauthlib

#url = "https://api.twitter.com/1.1/search/tweets.json?q=Apple&count=1"
url = "https://api.twitter.com/1.1/search/tweets.json?q=Apple&since=2016-12-26&max_id=1078266426329321472&count=1"

#import/lookup credentials
auth = requests_oauthlib.OAuth1(twitter_consumer_key, twitter_consumer_secret, twitter_access_token, twitter_access_secret)

#Send request to twitter URL
response = requests.get(url,auth = auth)
print(response)

#Format/marshall the response using json
jsonFormatted = response.json()
print(jsonFormatted)

#In the full json response, the actual tweet is in the status section, in the first part, called "text"
tweet = jsonFormatted["statuses"][0]["text"]
#print(tweet)

#The ID of each tweet
id_str = jsonFormatted["statuses"][0]["id_str"]
#print(id_str)

#The language of each tweet
lang = jsonFormatted["statuses"][0]["lang"]
print(lang)

#Build up a hash of keywords to look for
keywords = ["Halal", "halal"]

#iterate over all the responses
while len(jsonFormatted) != 0:

    statuses = jsonFormatted["statuses"]
    for i in range(len(statuses)):
        tweet =  statuses[i]["text"]
        id_str = statuses[i]["id_str"]
        lang = jsonFormatted["statuses"][0]["lang"]
        #if in a specific language, then look for certain keywords
        if lang == "en":
            for word in keywords:
                if word in tweet:
                    print("Found a tweet")
                    #print(lang)
                    #print(id_str)
                    print(tweet)
                    print(statuses[i]["created_at"])
    #Because twitter only allows you to retrieive the last 100 tweets, you can use the ID, to move back/forward in time, to get the latest 100 tweets over that persiod, using the max_id to move along
    url = "https://api.twitter.com/1.1/search/tweets.json?q=Apple&since=2016-12-26&max_id="+str(int(id_str)-1)+"&count=10"
    try: 
        response = requests.get(url,auth = auth)
        jsonFormatted = response.json()
    except:
        break